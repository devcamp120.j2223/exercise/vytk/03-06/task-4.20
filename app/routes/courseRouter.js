//Import thu vien express
const { response } = require("express");
const express=require("express");

const router=express.Router();

router.get("/courses",(request,response)=>{
    response.json({
        message:"GET ALL COURSE"
    })
});
router.post("/courses",(request,response)=>{
    response.json({
        message:"POST NEW COURSE"
    })
});
router.get("/courses/:courseId",(request,response)=>{
    let courseId=request.params.courseId;
    response.json({
        message:"GET COURSE ID: "+courseId
    })
});
router.put("/courses/:courseId",(request,response)=>{
    let courseId=request.params.courseId;
    response.json({
        message:"PUT COURSE ID: "+courseId
    })
});
router.delete("/courses/:courseId",(request,response)=>{
    let courseId=request.params.courseId;
    response.json({
        message:"DELETE COURSE ID: "+courseId
    })
});


//Export data file => module
module.exports=router;