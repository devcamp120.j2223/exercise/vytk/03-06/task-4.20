//Import thu vien express
const { response } = require("express");
const express=require("express");

const router=express.Router();

router.get("/reviews",(request,response)=>{
    response.json({
        message:"GET ALL REVIEWS"
    })
});
router.post("/reviews",(request,response)=>{
    response.json({
        message:"POST NEW REVIEWS"
    })
});
router.get("/reviews/:reviewsId",(request,response)=>{
    let reviewId=request.params.reviewId;
    response.json({
        message:"GET REVIEWS ID: "+reviewId
    })
});
router.put("/reviews/:reviewsId",(request,response)=>{
    let reviewId=request.params.reviewId;
    response.json({
        message:"PUT REVIEWS ID: "+reviewId
    })
});
router.delete("/reviews/:reviewsId",(request,response)=>{
    let reviewId=request.params.reviewId;
    response.json({
        message:"DELETE REVIEWS ID: "+reviewId
    })
});


//Export data file => module
module.exports=router;